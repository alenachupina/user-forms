import './App.css';
import Form from './Form/Form';


const sauceList = ["mustard", "ketchup", "mayonnaise", "guacamole"];
  const stoogeList = ["larry", "moe", "curly"];
  const initialState = {
    lastName: "",
    firstName: "",
    age: "",
    employed: false,
    favoriteColor: "",
    sauces: [],
    stooge: "larry",
    notes: "",
  };

function App() {
   return (
    <div className="App">
     <Form initialState={initialState} checkboxList={sauceList} radioList={stoogeList} />
  </div>
  );
}

export default App;
