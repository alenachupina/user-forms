import styles from './Button.module.css'
const Button = (props) =><button className={`${styles.button} ${styles[props.type]}`} type={props.type} disabled={props.disabled}>{props.text}</button>;

export default Button;