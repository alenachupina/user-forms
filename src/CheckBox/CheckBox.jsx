import styles from './CheckBox.module.css';
const CheckBox = ({label, name, value, onChange, checked}) =>{
  const handleChange = (e) => {
    onChange( e.target.value ? e.target.value : {[e.target.name]:e.target.checked} );
  }
    return (
        <label className={styles.label}>
        <span>{label}</span>
        <input
          className={styles.input}
          type="checkbox"
          name={name}
          onChange={handleChange}
          value={value}
          checked={checked}
        />
        </label>
            );
}

export default CheckBox;