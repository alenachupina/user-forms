import CheckBox from '../CheckBox';
import styles from './CheckBoxSet.module.css';
const CheckBoxSet = ({options,label, name, value, onChange}) =>{
  
  const handleChange = (current) => {
    const newList = [...value];
    const idx = newList.indexOf(current);
    if (idx === -1) {
          newList.push(current);
        } else {
          newList.splice(idx, 1);
        }
        onChange( {[name]:newList});
      }
    return (
        <div className={styles.outerLabel}>
       <span >{label}</span>
        <div>
            {options.map((opt)=><CheckBox key={opt}  name={name} label={opt} onChange={handleChange} value={opt} checked={value.includes(opt)} />)}
        </div>
       </div>
            );
}

export default CheckBoxSet;