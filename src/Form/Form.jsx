import styles from "./Form.module.css";
import React, { useState } from "react";
import Button from "../Button/Button";
import InfoBox from "../InfoBox/InfoBox";
import TextInput from "../TextInput/TextInput";
import CheckBox from "../CheckBox/CheckBox";
import Select from "../Select/Select";
import CheckBoxSet from "../CheckBox/CheckboxSet/CheckBoxSet";
import RadioBntSet from "../RadioBtn/RadioSet/RadioBtnSet";
import TextArea from "../Textarea/TextArea";

const Form = ({ checkboxList, initialState, radioList }) => {
  const [formState, setFormState] = useState(initialState);
  const [firstNameInputTouched, setFirstNameInputTouched] = useState(false);
  const [ageInputTouched, setAgeInputTouched] = useState(false);
  const [lastNameInputTouched, setLastNameInputTouched] = useState(false);
  const [formTouched, setFormTouched] = useState(false);

  const isLastNameValid = /^[A-Za-z\s]+$/.test(formState.lastName);
  const isFirstNameValid = /^[A-Za-z\s]+$/.test(formState.firstName);
  const isAgeValid = /^[0-9]+$/.test(formState.age);

  const isLastNameInputValid = !(!isLastNameValid && lastNameInputTouched);
  const isFirstNameInputValid = !(!isFirstNameValid && firstNameInputTouched);
  const isAgeInputValid = !(!isAgeValid && ageInputTouched);
  const isNotesInputValid = formState.notes.length <= 100;

  const isFormValid =
    isLastNameInputValid &&
    isFirstNameInputValid &&
    isAgeInputValid &&
    isNotesInputValid;
  const setAllInputsTouch = (value) => {
    setFirstNameInputTouched(value);
    setLastNameInputTouched(value);
    setAgeInputTouched(value);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (!(firstNameInputTouched && lastNameInputTouched && ageInputTouched)) {
      setAllInputsTouch(true);
    } else if (isFormValid) {
      window.alert(JSON.stringify(formState, 0, 2));
      resetHandler();
    }
  };
  const resetHandler = () => {
    setFormState(initialState);
    setAllInputsTouch(false);
    setFormTouched(false);
  };

  const handleChange = (val) => {
    if (!formTouched) {
      setFormTouched(true);
    }
    setFormState((prevState) => ({ ...prevState, ...val }));
  };

  return (
    <div className={styles.wrapper}>
      <form
        id="userForm"
        className={styles.form}
        onSubmit={submitHandler}
        onReset={resetHandler}
      >
        <TextInput
          name="firstName"
          label="First Name"
          value={formState.firstName}
          onChange={handleChange}
          isValid={isFirstNameInputValid}
          onTouch={setFirstNameInputTouched}
          required
          errorMsg="alphabetic characters only"
        />
        <TextInput
          name="lastName"
          label="Last Name"
          value={formState.lastName}
          onChange={handleChange}
          isValid={isLastNameInputValid}
          onTouch={setLastNameInputTouched}
          required
          errorMsg="alphabetic characters only"
        />
        <TextInput
          name="age"
          label="Age"
          value={formState.age}
          onChange={handleChange}
          isValid={isAgeInputValid}
          onTouch={setAgeInputTouched}
          required
          errorMsg="must be a number"
        />
        <CheckBox
          type="checkBox"
          name="employed"
          label="Employed"
          value=""
          checked={formState.employed}
          onChange={handleChange}
        />
        <Select
          options={["", "green", "red", "blue"]}
          name="favoriteColor"
          label="Favorite color"
          value={formState.favoriteColor}
          onChange={handleChange}
        />
        <CheckBoxSet
          options={checkboxList}
          name="sauces"
          label="Sauces"
          value={formState.sauces}
          onChange={handleChange}
        />
        <RadioBntSet
          options={radioList}
          checked={formState.stooge}
          name="stooge"
          label="Best stooge"
          onChange={handleChange}
        />
        <TextArea
          name="notes"
          label="Notes"
          value={formState.notes}
          onChange={handleChange}
          isValid={isNotesInputValid}
          errorMsg="too long"
        />
        <Button
          type="submit"
          text="submit"
          disabled={!isFormValid || !formTouched}
        />
        <Button type="reset" text="reset" disabled={!formTouched} />
        <InfoBox data={formState} />
      </form>
    </div>
  );
};
export default Form;
