import styles from './InfoBox.module.css'

const InfoBox = ({data}) => {
       const getOutput = () => {
      const result = {};
      for (const key in data){
       if(!(data[key].length === 0)){
           result[key] = data[key]
       }
      }
      return JSON.stringify(result,0, 2);
    }
    const output = getOutput(data)
    return (<pre className={styles.container}>
        {output}
    </pre>)
}

export default InfoBox;