import styles from './RadioBtn.module.css';
const RadioBtn = ({label, name, value, validate, onChange, checked}) =>{
  const handleChange = (e) => {
    onChange({[e.target.name]:e.target.value});
  }
    return (
        <label className={styles.label}>
        <span>{label}</span>
        <input
          className={styles.input}
          type="radio"
          name={name}
          onChange={handleChange}
          value={value}
          checked={checked === value}
        />
        </label>
            );
}

export default RadioBtn;