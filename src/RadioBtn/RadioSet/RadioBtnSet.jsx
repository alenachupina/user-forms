import styles from "./RadioBtnSet.module.css";
import RadioBtn from "./../RadioBtn";
const RadioBntSet = ({ options, label, name, onChange, checked }) => {
  return (
    <div className={styles.outerLabel}>
      <span>{label}</span>
      <div>
        {options.map((opt) => (
          <RadioBtn
            key={opt}
            name={name}
            label={opt}
            onChange={onChange}
            value={opt}
            checked={checked}
          />
        ))}
      </div>
    </div>
  );
};

export default RadioBntSet;
