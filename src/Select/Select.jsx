import styles from "./Select.module.css";
const Select = ({ options, label, name, value, validate, onChange }) => {
  const handleChange = (e) => {
    onChange({ [e.target.name]: e.target.value });
  };
  return (
    <label className={styles.label}>
      <span>{label}</span>
      <select
        className={styles.input}
        name={name}
        onChange={handleChange}
        value={value}
      >
        {options.map((opt) => (
          <option value={opt} key={opt}>
            {opt}
          </option>
        ))}
      </select>
    </label>
  );
};

export default Select;
