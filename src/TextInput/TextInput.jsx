import styles from './TextInput.module.css';
const TextInput = ({type, label, name, value, onChange, isValid, onTouch, errorMsg}) =>{
    
    const handleChange = (e) => {
      onChange({[e.target.name]:e.target.value});
    }
    const handleTouch = () =>{
       onTouch(true)
    }

    const error = () => {
     if (!isValid && value === ""){
       return "required"
     } else if (!isValid){
       return errorMsg
     }else{
       return ""
     }
    }

       return (
        <label className={styles.label}>
        <span>{label}</span>
        <input
         style={{
          border: isValid  ? "2px solid #ccc" : "2px solid red" 
         }}
          className={styles.input}
          type={type}
          name={name}
          onChange={ handleChange}
          onBlur={handleTouch}
          value={value}
          placeholder={label}
        />
        <span className={styles.error}>{error()}</span>
       
        </label>
            );
}

export default TextInput;
