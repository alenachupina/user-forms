import styles from './TextArea.module.css';
const TextArea = ({type, label, name, value, isValid, onChange, errorMsg, required}) =>{
  
  const handleChange = (e) => {
     onChange({[e.target.name]:e.target.value});
  }

  const error = () => {
    if (!isValid && value === "" && required){
      return "required"
    } else if (!isValid){
      return errorMsg
    }else{
      return ""
    }
   }
    return (
        <label className={styles.label}>
        <span>{label}</span>
        <textarea
        style={{
          border: isValid ? "2px solid #ccc" : "2px solid red"
         }}
          className={styles.input}
          type={type}
          name={name}
          onChange={handleChange}
          value={value}
          placeholder={label}
        />
         <span className={styles.error}>{error()}</span>
        </label>
            );
}

export default TextArea;
